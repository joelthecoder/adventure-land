var $ = parent.$;

if (!$('body').hasClass("jtd-ui-enabled")) {
    /************************/
    /* NEW LOOK FOR MENU BUTTONS
    /************************/

    $("#toprightcorner > div").each(function() {
        if ($(this).text() == "COM") 
            $(this).html('<ion-icon name="heart-outline"></ion-icon> Social').css({"border-color": "#e45656", "border-width":"2px", "border-style":"solid"});
    
         if ($(this).text() == "CHAR") 
            $(this).html('<ion-icon name="person-outline"></ion-icon> Char').css({"border-color": "#5f56e4", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "INV") 
            $(this).html('<ion-icon name="grid-outline"></ion-icon> Inv').css({"border-color": "#ea8823", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "STATS") 
            $(this).html('<ion-icon name="person-add-outline"></ion-icon> Stats').css({"border-color": "#48bd00", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "SKILLS") 
            $(this).html('<ion-icon name="star-outline"></ion-icon> Skills').css({"border-color": "#d658e4", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "GUIDE") 
            $(this).html('<ion-icon name="book-outline"></ion-icon> Guide').css({"border-color": "#a76d05", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "CODE") 
            $(this).html('<ion-icon name="code-outline"></ion-icon> Code').css({"border-color": "#4e4e4e", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "TRAVEL") 
            $(this).html('<ion-icon name="globe-outline"></ion-icon> Travel').css({"border-color": "#56b0e4", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "TOWN") 
            $(this).html('<ion-icon name="business-outline"></ion-icon> Town').css({"border-color": "#32ad7a", "border-width":"2px", "border-style":"solid"});
    
        if ($(this).text() == "CONF") 
            $(this).html('<ion-icon name="cog-outline"></ion-icon> Config').css({"border-color": "#adadad", "border-width":"2px", "border-style":"solid"});
    });

    /************************/
    /* CHAT INPUT PLACEHOLDER
    /************************/
    
    $('#chatinput').attr("placeholder", "Write something in chat...");

    /************************/
    /* MAKE CODEUI DRAGGABLE
    /************************/

    $("#codeui").draggable({ handle: ".CodeMirror-linenumber" });

    /************************/
    /* ADD IONICONS
    /************************/
    
    $('body').append(`
        <div class="jtd-resources">
            <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
        </div>
    `);
    
    /************************/
    /* CUSTOM CSS
    /************************/

    $('head').append(`
        <style class="jtd-css">
            /* MODAL */
        
            .modal .imodal {
                padding: 30px !important;
            }
        
            .modal .imodal .friendslist {
                border: 0 !important;
            }
            .modal .imodal .friendslist > table {
                margin: 20px 0px 0px 0px !important;
            }
        
            /* POSITIONS */
        
            #topleftcorner {top: 10px;left: 10px;}
            #toprightcorner {top: 10px;right: 10px;}
            #bottomleftcorner2 {bottom: 77px;left: 10px;}
            #bottomleftcorner {bottom: 360px;left: 10px;}
            #bottomrightcorner {bottom: 40px;right: 10px;}
        
            /* TARGET WINDOW */
        
            #topleftcorner #topleftcornerui .renderedinfo {
                border: 2px solid gray !important;
                background: rgba(0, 0, 0, 0.85) !important;
            }
        
            /* EQUIPPED EQ WINDOW */
        
            #topleftcorner #topleftcornerui .slots {
                margin-left: 10px !important;
            background: transparent !important;
            border: 0 !important;
            padding: 0 !important;
            }
        
            /* XP BAR */
        
            #bottomrightcorner > div:nth-child(3) {
                position: fixed !important;
                bottom: 0;
                left: 0;
                right: 0;
                width: auto !important;
                border: 0 !important;
                padding: 0 !important;
                margin-bottom: 0 !important;
                background: rgba(0, 0, 0, 0.85) !important;
            }
        
            /* XP BAR INNER */
        
            #bottomrightcorner > div:nth-child(3) > div {height:25px !important;}
            #bottomrightcorner > div:nth-child(3) > div:nth-child(1) {background: rgba(18, 255, 4, 0.45) !important;}
            #bottomrightcorner > div:nth-child(3) > div:nth-child(2) {
                line-height: 24px !important;
                width: 100% !important;
                top: 0 !important;
                font-size: 22px !important;
            }
        
            /* SKILL BAR CONTAINER */
            
            #bottomrightcorner > .bpclicks {
                bottom: 30px;
                position: relative;
            }
        
            /* SKILLBAR */
        
            #skillbar > div {background-color: transparent !important;border: 0 !important;}
        
            /* BOTTOMMID - CONTAINER */
        
            #bottommid {bottom: 40px !important;}
        
            /* BOTTOMMID - STATS*/
        
            #bottommid #rightcornerui {margin-bottom: 0px !important;width: 100% !important;}
            #bottommid #rightcornerui > div {
                border: 2px solid gray !important;
                width: 100%;
                box-sizing: border-box;
                margin-bottom: 20px !important;
                width: 100% !important;
            }
            #bottommid #rightcornerui > div > div {text-align:right;}
            #bottommid #rightcornerui > div > div span{float:left;padding:0px 20px 0px 0px;}
        
            /* BOTTOMMID - ATT & INV */
        
            #bottommid #rightcornerui + div {
                margin: 0 !important;
                border-bottom: 2px solid gray;
                border-left: 2px solid gray;
                border-right: 2px solid gray;
                border-top: 2px solid gray;
            }
            #bottommid #rightcornerui + div > div {
                border: 0 !important;
                padding: 0 !important;
                margin: 0 !important;
                height: 30px;
                width: 50%;
                box-sizing: border-box;
            }
        
            /* ATT METER */
        
            #bottommid #rightcornerui + div > div:nth-child(1) {
                border-right: 1px solid gray !important;
            }
        
            /* NAME PLATE */
        
            #bottommid #rightcornerui + div > div:nth-child(2) {
                display:none;
            }
        
            /* INV METER */
        
            #bottommid #rightcornerui + div > div:nth-child(3) {
                border-left: 1px solid gray !important;
            }
        
            /* INNER INV & ATT & NAME */
        
            #bottommid #rightcornerui + div > div > div {
                width: 100% !important;
                padding: 0px !important;
                font-size: 22px !important;
                line-height: 28px !important;
                background: rgb(0, 0, 0) !important;
            }
        
            /* INNER INV & ATT & NAME LOADING */
        
            #bottommid #rightcornerui + div > div > div > div {
                background: gray !important;
            }
        
            /* HP METER */
        
            #bottommid > div:nth-child(7) {
                border: 0 !important;
                padding:0 !important;
                margin: 0 !important;
                border-left: 2px solid gray !important;
                border-bottom: 2px solid gray !important;
            }
        
            /* MP METER */
        
            #bottommid > div:nth-child(8) {
                border: 0 !important;
                padding: 0 !important;
                margin: 0 !important;
                border-right: 2px solid gray !important;
                border-bottom: 2px solid gray !important;
                border-left: 2px solid gray !important;
            }
        
            /* HP & MP METER INNER */
        
            #bottommid > div:nth-child(7) > div:nth-child(2),
            #bottommid > div:nth-child(8) > div:nth-child(2){
                top: 0 !important;
                bottom: 0 !important;
                font-size: 22px !important;
                line-height: 28px !important;
            }
        
            /* CHAT */
        
            #bottomleftcorner2 > div:nth-child(1) {
                border: 2px solid gray !important;
                margin-bottom: -2px !important;
            }
            #chatlog {
                border: 2px solid gray;
                background: rgba(0, 0, 0, 0.85) !important;
            }   
            #chatinput {
                bottom: 40px;
                left: 10px;
                border: 2px solid gray;
                background: rgba(0, 0, 0, 0.85) !important;
                text-indent:5px;
            }
        
            /* GAMELOG */
        
            #gamelog {border: 2px solid gray;background: rgba(0, 0, 0, 0.85) !important;}
        
            /* BUTTONS */
        
            .gamebutton {
                color: white;
                border: 2px solid gray;
                padding: 10px 15px;
                border-radius: 5px;
        
                vertical-align: middle;
                -webkit-transform: perspective(1px) translateZ(0);
                transform: perspective(1px) translateZ(0);
                box-shadow: 0 0 1px rgba(0, 0, 0, 0);
                -webkit-transition-duration: 0.3s;
                transition-duration: 0.3s;
                -webkit-transition-property: transform;
                transition-property: transform;
            }
        
            .gamebutton:hover, .gamebutton:focus, .gamebutton:active {-webkit-transform: scale(1.1);transform: scale(1.1);}
            .gamebutton .md {font-size: 21px;vertical-align: top;margin: 2px 0px 0px 0px;}
        
            /* CODE EDITOR */
            #codeui .CodeMirror-linenumber {
                cursor:move;
            }
            #codeui {
                resize: both;
                overflow: auto;
                padding: 0;
        
                top: 10px;
                left: 10px;
                bottom: 10px;
                border: 0;
                max-height: calc(100vh - 50px);
                max-width: calc(100vw - 20px);
        
                min-height: 100px;
                min-width: 200px;
        
                -webkit-box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.75);
                -moz-box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.75);
                box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.75);
            }
        
            #codeui .gamebutton {right: 30px;bottom: 15px;}
        
            /* INVENTORY */
        
            .theinventory {border: 2px solid gray !important;}
            .theinventory > div:nth-child(3) {border-bottom: 2px solid gray !important; margin: 2px -2px 3px -2px !important;}
            .theinventory div div[data-cnum] {border: 2px solid #1f1f1f !important;}
            .theinventory div div[data-cnum] div[data-inum] {border: 2px solid #808080 !important;}
        
            /* SKILLS */
        
            #theskills {bottom: 40px !important;right: 10px !important;}
            #theskills > div:nth-child(2) {border: 2px solid gray !important;padding: 10px !important;}
            #skills-item > div {border: 2px solid gray !important;}
        
            /* CODE RUNNING NOTIFICATION */
        
            iframe#maincode {
                border: 2px solid gray !important;
                background-color: transparent;
                position: fixed;
                right: 10px;
                top: 180px;
                animation: blinker 1s linear infinite;
            }
        
            @keyframes blinker {50% {opacity: 0;}}
        </style>
    `);

    /************************/
    /* ADD BODY CLASS
    /************************/

    $('body').addClass("jtd-ui-enabled");

    log("---- JTD Custom UI Enabled ----");
    log("Uninstallation: Remove the snippet in the code window and restart / refresh game.");
}